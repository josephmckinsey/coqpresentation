\documentclass{lug}
\usepackage{fontspec}
\usepackage[backend=bibtex]{biblatex}
\addbibresource{cites.bib}

\usepackage{bussproofs}
\usepackage{multicol}

\title{A short introduction to formal mathematics in coq}
\author{Joseph McKinsey: Colorado School of Mines Linux Users Group}

\begin{document}

% I'll go over some of the basics of formal logic and the types of strongly
% typed functional programming. Then I'll show how Coq uses this to express
% theorems and proofs. The rest of the talk will mainly be going over some
% examples of how you actually prove thing: how you express the integers, use
% induction, and other common "tactics". Somewhere in all this is going to be
% something on the excluded middle.

\begin{frame}{What is Coq?}
  The What?
  \begin{itemize}[<+->]
  \item Strongly Typed Functional Programming Language
  \item Dependent Types
  \item Proof Assistant via ``tactics''
  \end{itemize}
  The Why?
  \begin{itemize}[<+->]
  \item Formalized mathematical proofs
    \begin{itemize}
      \item Four-color Map Theorem \cite{fourcolor}
      \item G\"odel's proof of god \cite{godelsproofofgod}
      \item Topology \cite{topology}
    \end{itemize}
  \item Verified C-compiler \cite{compcert}
  \end{itemize}
\end{frame}

\section{Formal Logic}

\begin{frame}[fragile]{Axiom of Choice}
  
\begin{minted}{coq}
Axiom func_choice : forall A B (R : A -> B -> Prop),
  (forall x, exists y, R x y) ->
  exists f, forall x, R x (f x).
\end{minted}

\pause

\begin{center}
Just kidding!
\end{center}

\end{frame}

\begin{frame}[fragile]{Logical Connectives}
  A lot of the logic you learn in Discrete Mathematics is about booleans:

  \begin{tabular}{c|c|c|c}
    Connector & Meaning & \texttt{C++} & \texttt{Coq} (Prop) \\ \hline
    $x \land y$ & both must be true & \mintinline{c++}{&&} & \mintinline{coq}{/\} \\
    $x \lor y$ & one or both must be true & \mintinline{c++}{||} & \mintinline{coq}{\/} \\
    $\lnot x$ & not & \mintinline{c++}{!} & \mintinline{coq}{~} \\
    $x \oplus y$ & one or the other, but not both & \mintinline{c++}{^} & \\
    $x \to y$ & if $x$ is true, then $y$ is true & \mintinline{c++}{!x || y} & \mintinline{coq}{->}
  \end{tabular}

  Coq uses these same connectors but with some different rules.
\end{frame}

\begin{frame}[fragile]{Variables and Functions}
  We also allow free variables such as $x$ and functions like $P(x)$. In Coq,
  our function might be \mintinline{coq}{even} and then function application is
  \mintinline{coq}{even x}.

  These are our \textbf{terms}.
\end{frame}

\begin{frame}[fragile]{Logical Quantifiers}
  We also have logical \textbf{quantifiers} that allow us to state more complex
  facts.

  \begin{tabular}{c|c|c}
    Quantifier & Meaning & Coq \\ \hline
    $\forall x P(x)$ & $P(x)$ is true if you give it any $x$. & \mintinline{coq}{forall x . even x} \\
    $\exists x P(x)$ & $P(x)$ is true for some $x$. & \mintinline{coq}{exists x . even x}
  \end{tabular}

  This brings us to what we call ``first order logic''.
\end{frame}

\begin{frame}[fragile]{Terms, Sentences, and Formulas. Oh My!}
  In formal logic (and in Coq), we can build up \textbf{terms} using variables and
  functions. Then we can build \textbf{formulas} or \textbf{sentences} using
  connectors and quantifiers.
  
  \[
    \forall x Even(x) \to \exists k (x = k + k)
  \]
  \begin{center}
    \mintinline{coq}{forall x . even x -> exists k . (x = k + k)}
  \end{center}
\end{frame}

\begin{frame}[fragile]{Logical Deduction}
  If we were working with only booleans, we could use ``truth tables'' and
  evaluate the truth of basic propositions by trying every value. Formal logic
  instead uses only logical deduction: \textbf{rules that let us transform
  and combine sentences}.

  Here are some examples.
  \begin{prooftree}
    \AxiomC{$P$}
    \AxiomC{$Q$}
    \BinaryInfC{$P \land Q$}
  \end{prooftree}

  \begin{prooftree}
    \AxiomC{$P$}
    \AxiomC{$P \to Q$}
    \BinaryInfC{$Q$}
  \end{prooftree}

  \begin{prooftree}
    \AxiomC{$P$}
    \AxiomC{$P \lor Q \to R$}
    \BinaryInfC{$R$}
  \end{prooftree}
\end{frame}

\begin{frame}[fragile]{What about $=$?}
  Equality is going to be an \textbf{atomic} connector.
  \begin{itemize}
  \item It creates a proposition that is ``provable'' if and only if the
    two sides are \textbf{exactly} equal.
    Think of it as a type that you can only instantiate if both sides have the
    same value.
  \item You can use equality to rewrite formulas and functions.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{A note on true and false}
  Discrete mathematics uses $\mathrm{T}$ and $\mathrm{F}$, but in formal logic
  you use $\top$ and $\bot$.

  In Coq, you should keep the idea of booleans and logical truthiness separate.
  They are in fact different and some people believe that some facts about
  boolean logic should \text{not} hold about logical truths.

  Coq has the type \textbf{bool} and the type \textbf{Prop}. They are not
  interchangeable. Logical truthiness $\top$ is \textbf{True} and logical false
  $\bot$ is \textbf{False}. More on this later.
  
\end{frame}

% I'll go over some of the basics of formal logic and the types of strongly
% typed functional programming. Then I'll show how Coq uses this to express
% theorems and proofs. The rest of the talk will mainly be going over some
% examples of how you actually prove thing: how you express the integers, use
% induction, and other common "tactics". Somewhere in all this is going to be
% something on the excluded middle.


\section{Strongly Typed Functional Programming}

\begin{frame}[fragile]{Functional Programming}
  TLDR:
  
  Functions can be passed between functions and operated on.

  Recursion is good and for-loops are bad.
\end{frame}

\begin{frame}[fragile]{Currying}
  You \textit{could} have a function that takes two values and spits out one:
  \begin{minted}{coq}
    f : A * B -> C
  \end{minted}
  \text{or} you could split it up into a function that returns a function:
  \begin{minted}{coq}
    f1 : A -> (B -> C)
    f2 : A -> B -> C
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Strongly Typed}
  Some functional languages don't even have any types at all!

  Luckily, we do have types. Even better, these types are all checked
  at compile time (so it is statically typed as well).

  This will allow some weird constructions that some people find helpful
  (see monads).
\end{frame}

\section{Dependent Types}

\begin{frame}[fragile]{Dependent Types}
  There's a lot of crazy notation and rules involved with this that are beyond
  the scope of this presentation. But the gist is that you can use
  $\exists$ and $\forall$ in your types.

  This allows you to do some cool types that depend on other values, but we
  won't be using all of that. The only thing you need to know is that

  \mintinline{coq}{forall} and \mintinline{coq}{exists} can be used in your
  types and they behave like you expect.
\end{frame}


\begin{frame}[fragile]{Using Dependent Types to Express Logical Facts}
  We have a type called \mintinline{coq}{Prop} where the type is ``inhabited''
  by proofs of that proposition. We can use all logical connectives and 
  quantifiers to build up propositions.

  So then we can write our first valid Coq theorem.
  \begin{minted}{coq}
    Theorem or_fact : forall (P : Prop) (Q : Prop),
      P -> P \/ Q.
  \end{minted}

  We'll prove this later.
\end{frame}

\begin{frame}[fragile]{The Curry-Howard Correspondence}
  Fancy people call the previous fact the ``Curry-Howard Correspondence''.
\end{frame}

\begin{frame}[fragile]{True and False again}
  So if your propositions are types, then what's $\top$ and $\bot$?

  \pause

  They are called \mintinline{coq}{True} and \mintinline{coq}{False}.

  They are defined as a type inhabited by one element and a type inhabited by no
  element, given by the constructor \mintinline{coq}{I}.
\end{frame}

\begin{frame}[fragile]{What is not?}
  If a proposition $P$ is false, then we represent that fact by
  \begin{minted}{coq}
    P -> False
  \end{minted}

  If we had a proof of $P$, then \mintinline{coq}{False} would then be
  inhabited, this leads to a contradiction.

  Coq will allow us to prove anything if we have a contradiction. We'll see this
  in a proof later.
\end{frame}

\section{Data Types in Coq}

\begin{frame}[fragile]{Data Types}
  How do we build types in Coq?

  \begin{itemize}[<+->]
  \item Enumeration (Union types)
  \item Combination (Product types)
  \item Recursion
  \item Some combination of the previous three
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Union Types}
  \begin{minted}{coq}
Inductive bool : Type :=
| true
| false.

Inductive either (a : Type) (b : Type) : Type :=
| left (x : a)
| right (x : a).

Inductive either' (a : Type) (b : Type) : Type :=
| left' : a -> (either' a b)
| right' : b -> (either' a b).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Product types}
  \begin{minted}{coq}
Inductive both (a : Type) (b : Type) : Type :=
  bothConstructor (x : a) (y : b).

Inductive both' (a : Type) (b : Type) : Type :=
  bothConstructor' : a -> b -> (both' a b).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Recursive Data Types}
  \begin{minted}{coq}
Inductive list (a : Type) : Type :=
| nil
| cons (h : a) (t : list a).

Inductive list' (a : Type) : Type :=
| nil'
| cons' : a -> list' a -> list' a.
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Recursive Data Types (Part II)}
  \begin{minted}{coq}
Inductive tree (v : Type) : Type :=
| leaf
| branch (l : (tree v)) (r : (tree v)) (x : v).

Inductive tree' (v : Type) : Type :=
| leaf'
| branch' (l : (tree v)).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Recursive Data Types (Part III)}
  \begin{minted}{coq}
Inductive nat : Type :=
| O
| S (n : nat).

Inductive nat' : Type :=
| O'
| S' : nat' -> nat'.
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Recursive Data Types (Part IV)}
  \begin{minted}{coq}
Inductive reg_exp {T : Type} : Type :=
  | EmptySet
  | EmptyStr
  | Char (t : T)
  | App (r1 r2 : reg_exp)
  | Union (r1 r2 : reg_exp)
  | Star (r : reg_exp).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Recursive Data Types (Part V)}
  There are also inductive propositions, but we won't come back to these.
  \begin{minted}{coq}
Inductive even : nat -> Prop :=
| ev_0 : even O
| ev_SS (n : nat) (H : even n) : even (S (S n)).
  \end{minted}
  These have some nice properties, but have their own frustrations.
\end{frame}

\section*{Functions!}

\begin{frame}[fragile]{Basic Definitions and Pattern Matching}
  \begin{minted}{coq}
Definition negb (x : bool) : bool :=
  match x with
    | true => false
    | false => true
  end.

Definition andb (x : bool) (y : bool) : bool :=
  match x, y with 
  | false, false => false
  | true, false => false
  | false, true => false
  | true, true => true
  end.
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Termination Checking and Recursion}
  \begin{itemize}
  \item Addition!
  \item Minus!
  \item Division!?
  \item Trees!
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Addition}
  \begin{minted}{coq}
Fixpoint plus (x : nat) (y : nat) : nat :=
  match x with
  | O => y
  | S x' => S (plus x' y)
  end.

Compute plus (S (S (S (S O)))) (S (S (S O))).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Subtraction}
  \begin{minted}{coq}
Fixpoint minus (x : nat) (y : nat) : nat :=
  match y with
  | O => x
  | S y' => match (minus x y') with
    | O => O
    | S result => result
    end
  end.

Compute minus (S (S (S (S O)))) (S (S (S O))).
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Division}
  \begin{minted}{coq}
Fixpoint div (x : nat) (d : nat) : nat :=
  match x with
  | O => O
  | x => S (div (minus x d) d)
  end.
  \end{minted}
  Does this work?
\end{frame}

\begin{frame}[fragile]{Why not?}
  The termination checker needs a decreasing argument.

  Remember that all types are either enumerations, products, or inductive!
\end{frame}

\begin{frame}[fragile]{Tree Sums}
\begin{minted}{coq}
Fixpoint sumTree (t : tree nat) : nat :=
  match t with
  | leaf _ => O
  | branch _ l r x => plus (plus (sumTree l) (sumTree r)) x
  end.
\end{minted}

So recursion is still useful.
\end{frame}

\section*{Proofs!}

\begin{frame}[fragile]{Tactics}
  They are the rules you use to actually proof things.
  \begin{multicols}{2}
  \begin{itemize}
  \item intros
  \item reflexivity
  \item simpl
  \item destruct
  \item induction
  \item rewrite
  \item assert
  \item apply
  \item discriminate
  \item exfalso
  \item inversion
  \item exists
  \item omega (the magic one)
  \end{itemize}
  \end{multicols}
  And many more. You can also write your own!
\end{frame}

\begin{frame}[fragile]{Propositions}
  \begin{minted}{coq}
Theorem or_fact : forall (P : Prop) (Q : Prop),
  P -> P \/ Q.
  \end{minted}
  I promised I would prove this.
\end{frame}

\begin{frame}[fragile]{Integers}
  \begin{itemize}
    \item Addition has two-sided identity.
    \item Addition is associative
    \item Subtraction is an inverse.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Induction}
  You can decompose something into its constructors, and you assume that it
  holds for all smaller structures.

  So for integers, we decompose $n$ into
  \begin{itemize}
    \item $n = 0$. \quad Now we prove the base case...
    \item $n > 0$. \quad Assuming that it is true for $n$, now prove it for $n +
      1$.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Induction Requirements}
  \begin{itemize}
  \item Different constructors always produce different results.
  \item You can always apply a constructor if it fits.
  \item It's injective!
    If \mintinline{coq}{Constructor a = Constructor b}, then \mintinline{coq}{a = b}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Trees}
  \begin{itemize}
  \item Anything you want me to prove?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Want to learn more?}
  \begin{center}
  \includegraphics[width=0.5\textwidth]{lf_icon} \cite{softwarefoundations}
  \end{center}
\url{https://softwarefoundations.cis.upenn.edu/current/index.html}
\end{frame}

\begin{frame}[allowframebreaks]{References}
  \printbibliography
\end{frame}

\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-engine: xetex
%%% End: