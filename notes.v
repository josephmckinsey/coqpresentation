Theorem neg_involutive : forall (P : Prop) (Q : Prop),
  P -> P \/ Q.
Proof.
  intros P Q.
  intros H.
  left. apply H. Qed.

Inductive bool : Type :=
| true
| false.

Inductive either (a : Type) (b : Type) : Type :=
| left (x : a)
| right (x : a).

Inductive either' (a : Type) (b : Type) : Type :=
| left' : a -> (either' a b)
| right' : b -> (either' a b).

Inductive both (a : Type) (b : Type) : Type :=
  bothConstructor (x : a) (y : b).

Inductive both' (a : Type) (b : Type) : Type :=
  bothConstructor' : a -> b -> (both' a b).

Inductive list (a : Type) : Type :=
| nil
| cons (h : a) (t : list a).

Inductive list' (a : Type) : Type :=
| nil'
| cons' : a -> list' a -> list' a.

Inductive tree (v : Type) : Type :=
| leaf
| branch (l : (tree v)) (r : (tree v)) (x : v).

Inductive tree' (v : Type) : Type :=
| leaf'
| branch' (l : (tree v)).

Inductive nat : Type :=
| O
| S (n : nat).

Inductive nat' : Type :=
| O'
| S' : nat' -> nat'.

Inductive even : nat -> Prop :=
| ev_0 : even O
| ev_SS (n : nat) (H : even n) : even (S (S n)).

Inductive reg_exp {T : Type} : Type :=
  | EmptySet
  | EmptyStr
  | Char (t : T)
  | App (r1 r2 : reg_exp)
  | Union (r1 r2 : reg_exp)
  | Star (r : reg_exp).

Definition negb (x : bool) : bool :=
  match x with
    | true => false
    | false => true
  end.

Definition andb (x : bool) (y : bool) : bool :=
  match x, y with 
  | false, false => false
  | true, false => false
  | false, true => false
  | true, true => true
  end.

Fixpoint plus (x : nat) (y : nat) : nat :=
  match x with
  | O => y
  | S x' => S (plus x' y)
  end.

Compute plus (S (S (S (S O)))) (S (S (S O))).

Fixpoint minus (x : nat) (y : nat) : nat :=
  match y with
  | O => x
  | S y' => match (minus x y') with
    | O => O
    | S result => result
    end
  end.

Compute minus (S (S (S (S O)))) (S (S (S O))).

Fixpoint div (x : nat) (d : nat) : nat :=
  match x with
  | O => O
  | x => S (div (minus x d) d)
  end.

Fixpoint sumTree (t : tree nat) : nat :=
  match t with
  | leaf _ => O
  | branch _ l r x => plus (plus (sumTree l) (sumTree r)) x
  end.