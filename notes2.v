Theorem or_fact : forall (P : Prop) (Q : Prop),
  P -> P \/ Q.
Proof.
  intros P Q.
  intros H.
  left.
  apply H.
  Qed.

Theorem plus_O_n : forall n : nat,
  O + n = n.
Proof.
  intros n.
  simpl.
  reflexivity.
  Qed.

Theorem plus_n_O' : forall n : nat,
  n + O = n.
Proof.
  intros n.
  simpl.
  destruct n as [|n'].
  - simpl. reflexivity.
  - simpl. simpl.
  Abort.

Theorem plus_n_O : forall n : nat,
  n + O = n.
Proof.
  intros n.
  simpl.
  unfold plus.
  induction n as [|n' Ihn'].
  - simpl. reflexivity.
  - simpl. rewrite -> Ihn'.
    reflexivity.
  Qed.

Theorem minus_minus_is_O : forall n : nat,
  n - n = O.
Proof.
  intros n.
  simpl.
  induction n as [|n' Ihn'].
  - simpl. reflexivity.
  - simpl. rewrite Ihn'. reflexivity.
  Qed.

Theorem n_Sm_is_Sn_m : forall (n m : nat),
  n + S m = S n + m.
Proof.
  intros n m.
  induction n as [|n' Ihn'].
  - simpl. reflexivity.
  - simpl. rewrite Ihn'. simpl. reflexivity.
  Qed.

Theorem plus_minus_is_O : forall (n m : nat),
  (n + m) - m = n.
Proof.
  intros n m.
  induction m as [|m' Ihm'].
  - simpl. rewrite -> plus_n_O.
    simpl. destruct n as [|n'].
    * simpl. reflexivity.
    * simpl. reflexivity.
  - simpl. rewrite -> n_Sm_is_Sn_m. simpl.
    apply Ihm'.
  Qed.

Theorem plus_associative : forall (x y z : nat),
  (x + y) + z = x + (y + z).
Proof.
  intros x y z.
  induction x as [|n' Ihn'].
  - simpl. reflexivity.
  - simpl. rewrite <- Ihn'. reflexivity.
  Qed.